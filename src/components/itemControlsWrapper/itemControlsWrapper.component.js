import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export class ItemControlsWrapper extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
  };

  render() {
    const { children, onKeyboardFocus, ...rest } = this.props;
    return (
      <span {...rest}>
        {children}
      </span>
    )
  }
}
