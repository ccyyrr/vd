import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import TextField from 'material-ui/TextField';
import ModeEdit from 'material-ui/svg-icons/editor/mode-edit';
import { green300 } from 'material-ui/styles/colors';

import { getActions } from '../../helpers/getActions';
import { validateText } from '../../helpers/validate';

export const CUSTOM_INPUT_STYLE = {
  width: '100%',
};
export const CUSTOM_BUTTON_STYLE = {
  marginRight: 10,
};

export class EditModal extends PureComponent {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    data: PropTypes.object,
  };
  
  state = {
    isDisabled: true,
    error: '',
    value: this.props.data.get('name'),
    isOpen: false,
  };

  handleOpen = () => this.setState({ isOpen: true, value: this.props.data.get('name'), isDisabled: true });

  handleCancel = () => this.setState({ isOpen: false });

  handleSubmit = () => {
    this.props.onSubmit({
      name: this.state.value,
      id: this.props.data.get('id'),
      todo_list: this.props.data.get('todo_list'),
    });
    this.setState({ isOpen: false });
  };

  render() {
    const { data } = this.props;
    return (
      <FloatingActionButton
        style={CUSTOM_BUTTON_STYLE}
        backgroundColor={green300}
        mini={true}
        onClick={this.handleOpen}
      >
        <ModeEdit />
        <Dialog
          title={`Change name  "${data.get('name')}" to something else` }
          actions={getActions(this.handleCancel, this.handleSubmit, this.state.isDisabled)}
          modal={false}
          open={this.state.isOpen}
          onRequestClose={this.handleClose}
        >
          <TextField
            hintText="Title"
            value={this.state.value}
            errorText={this.state.error}
            style={CUSTOM_INPUT_STYLE}
            onChange={e => validateText(e, this.setState.bind(this))}
          />
        </Dialog>
      </FloatingActionButton>
    );
  }
}