import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';

export const CUSTOM_TEXT_FIELD_STYLE = {
  width: '100%',
  margin: '0 auto',
};

export class Search extends PureComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
  };

  handleChange = ({ target: { value }}) => this.props.onChange(value);

  render() {
    return (
      <TextField
        style={CUSTOM_TEXT_FIELD_STYLE}
        floatingLabelText="Search"
        onChange={this.handleChange}
      />
    );
  }
}
