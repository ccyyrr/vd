import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ActionAssignment from 'material-ui/svg-icons/action/assignment';
import DeleteForever from 'material-ui/svg-icons/action/delete';
import { blue500, red200 } from 'material-ui/styles/colors';

import{ EditModal } from '../../../components/editModal/editModal.component';
import{ ItemControlsWrapper } from '../../../components/itemControlsWrapper/itemControlsWrapper.component';

export class TodoListsItem extends PureComponent {
  static propTypes = {
    removeTodoList: PropTypes.func.isRequired,
    updateTodoList: PropTypes.func.isRequired,
    data: PropTypes.object,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  render() {
    const { data, removeTodoList, updateTodoList, history } = this.props;
    return (
      <ListItem
        leftAvatar={<Avatar icon={<ActionAssignment />} backgroundColor={blue500} />}
        onClick={() => { history.push(`/list/${data.get('id')}`) }}
        rightIconButton={
          <ItemControlsWrapper>
            <EditModal
              data={data}
              onSubmit={updateTodoList}
            />
            <FloatingActionButton
              backgroundColor={red200}
              mini={true}
              onClick={() => removeTodoList(data.get('id'))}
            >
              <DeleteForever />
            </FloatingActionButton>
          </ItemControlsWrapper>
        }
        primaryText={data.get('name')}
        secondaryText={`Todos count: ${data.get('todos_count')}`}
      />
    );
  }
}
