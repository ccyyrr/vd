import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { withRouter } from 'react-router-dom'

import { TodoListsItem } from './todoListsItem.component';
import { TodoActions } from '../../../modules/todo/todo.redux.js';

const mapStateToProps = createStructuredSelector({
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  removeTodoList: TodoActions.removeTodoList,
  updateTodoList: TodoActions.updateTodoList,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TodoListsItem));
