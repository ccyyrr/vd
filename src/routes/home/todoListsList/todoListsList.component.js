import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { List } from 'material-ui/List';
import TodoListsItem from '../todoListsItem/todoListsItem.container';

export class TodoListsList extends PureComponent {
  static propTypes = {
    data: PropTypes.object,
  };

  renderItems = dataset => dataset
    .map((item, index) => <TodoListsItem key={index} data={item}/>);

  render() {
    const { data } = this.props;
    return (
      <List>
        {this.renderItems(data)}
      </List>
    );
  }
}
