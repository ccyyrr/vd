import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import TextField from 'material-ui/TextField';

import { getActions } from '../../../helpers/getActions';
import { validateText } from '../../../helpers/validate';

export const CUSTOM_INPUT_STYLE = {
  width: '100%',
};
export const BUTTON_STYLE = {
  marginRight: 40,
  marginLeft: 15,
};

export class AddTodoModal extends PureComponent {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
  };

  state = {
    isDisabled: true,
    error: '',
    value: '',
    isOpen: false,
  };

  handleOpen = () => this.setState({ isOpen: true, isDisabled: true });

  handleCancel = () => this.setState({ isOpen: false });

  handleSubmit = () => {
    this.props.onSubmit({ name: this.state.value });
    this.setState({ isOpen: false });
  };

  render() {
    return (
      <div>
        <FloatingActionButton
            mini={true}
            style={BUTTON_STYLE}
            onClick={this.handleOpen}
        >
          <ContentAdd />
        </FloatingActionButton>
        <Dialog
          title="Add new Todo List"
          actions={getActions(this.handleCancel, this.handleSubmit, this.state.isDisabled)}
          modal={false}
          open={this.state.isOpen}
          onRequestClose={this.handleClose}
        >
          <TextField
            hintText="Title"
            errorText={this.state.error}  
            style={CUSTOM_INPUT_STYLE}
            onChange={e => validateText(e, this.setState.bind(this))}
          />
        </Dialog>
      </div>
    );
  }
}