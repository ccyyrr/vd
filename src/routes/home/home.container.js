import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Home } from './home.component';
import { TodoActions } from '../../modules/todo/todo.redux.js';
import { selectFilteredLists } from '../../modules/todo/todo.selectors.js';

const mapStateToProps = createStructuredSelector({
  todoLists: selectFilteredLists,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  fetchTodoLists: TodoActions.fetchTodoLists,
  addTodoList: TodoActions.addTodoList,
  setFilter: TodoActions.setFilter,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);