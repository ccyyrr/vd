import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import Paper from 'material-ui/Paper';
import { TodoListsList } from './todoListsList/todoListsList.component';
import { Search } from '../../components/search/search.component';
import { AddTodoModal } from './addTodoModal/addTodoModal.component'

export const CUSTOM_PAPER_STYLE = { width: 400, padding: 20 };

export class Home extends PureComponent {
  static propTypes = {
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
    fetchTodoLists: PropTypes.func.isRequired,
    addTodoList: PropTypes.func.isRequired,
    setFilter: PropTypes.func.isRequired,
    todoLists: PropTypes.object,
  };

  componentWillMount() {
    this.props.fetchTodoLists();
  }

  render() {
    const { todoLists, addTodoList, setFilter } = this.props;
    return (
      <div className={'home'}>
        <Helmet title="Homepage" />
          <div className={'home__header'}>
            Todo Lists
          </div>
          <Paper style={CUSTOM_PAPER_STYLE}>
            <div className={'home__controls'}>
              <AddTodoModal onSubmit={addTodoList} />
              <Search onChange={value => setFilter({ name: value })} />
            </div>
            <TodoListsList data={todoLists} />
          </Paper>
      </div>
    );
  }
}
