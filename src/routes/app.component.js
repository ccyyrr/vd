import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

export class App extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
    match: PropTypes.object.isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
    location: PropTypes.object.isRequired,
  };
  render() {
    return (
      <div className={'app'}>
        <Helmet
          titleTemplate="%s - Todo"
          defaultTitle="Todo"
        />
        {React.Children.only(this.props.children)}
      </div>
    );
  }
}
