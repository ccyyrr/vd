import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { List } from 'material-ui/List';
import TodoItem from '../todoItem/todoItem.container';

export class TodoList extends PureComponent {
  static propTypes = {
    data: PropTypes.object,
  };

  renderItems = dataset => dataset
    .map((item, index) => <TodoItem key={index} data={item}/>);

  render() {
    const { data } = this.props;
    return (
      <List>
        {this.renderItems(data)}
      </List>
    );
  }
}
