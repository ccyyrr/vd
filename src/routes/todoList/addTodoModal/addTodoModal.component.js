import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import Done from 'material-ui/svg-icons/action/done';
import CropDin from 'material-ui/svg-icons/image/crop-din';

import { getActions } from '../../../helpers/getActions';
import { validateText } from '../../../helpers/validate';

export const CUSTOM_BUTTON_STYLE = {
  width: '100%',
};
export const CUSTOM_INPUT_STYLE = {
  width: '80%',
};
export const CUSTOM_CHECKBOX_STYLE = {
  width: 'auto',
  display: 'block',
};

export class AddTodoModal extends PureComponent {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    listId: PropTypes.string.isRequired,
  };

  state = {
    isDisabled: false,
    error: '',
    value: '',
    isOpen: false,
    is_complete: false,
  };

  handleOpen = () => this.setState({
    isOpen: true,
    isDisabled: true,
    value: '',
    is_complete: false,
    error: ''
  });

  handleCancel = () => this.setState({ isOpen: false });

  handleCheck = ({ target: { checked }}) => this.setState({ is_complete: checked });

  handleSubmit = () => {
    this.props.onSubmit({
      name: this.state.value,
      is_complete: this.state.is_complete,
      todo_list: this.props.listId
    });
    this.setState({ isOpen: false });
  };

  render() {
    return (
      <div>
        <RaisedButton
          style={CUSTOM_BUTTON_STYLE}
          label="Add new Todo"
          onClick={this.handleOpen}
        />
        <Dialog
          title="Add new Todo"
          actions={getActions(this.handleCancel, this.handleSubmit, this.state.isDisabled)}
          modal={false}
          open={this.state.isOpen}
          onRequestClose={this.handleClose}
        >
          <div className={'add-todo-modal__content'}>
            <Checkbox
              style={CUSTOM_CHECKBOX_STYLE}
              checkedIcon={<Done />}
              uncheckedIcon={<CropDin />}
              onCheck={this.handleCheck}
            />
            <TextField
              hintText="Title"
              style={CUSTOM_INPUT_STYLE}
              errorText={this.state.error}
              onChange={e => validateText(e, this.setState.bind(this))}
            />
          </div>
        </Dialog>
      </div>
    );
  }
}