import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import Paper from 'material-ui/Paper';
import { TodoList } from './todoList/todoList.component';
import { Search } from '../../components/search/search.component';
import { AddTodoModal } from './addTodoModal/addTodoModal.component';
import { TodoFilter } from './todoFilter/todoFilter.component';

export const CUSTOM_PAPER_STYLE = { width: 400, padding: 20 };

export class TodoListView extends PureComponent {
  static propTypes = {
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
    addTodo: PropTypes.func.isRequired,
    setFilter: PropTypes.func.isRequired,
    fetchTodos: PropTypes.func.isRequired,    
    todos: PropTypes.object,
    match: PropTypes.object,
  };

  componentWillMount() {
    this.props.fetchTodos(this.props.match.params.listId);
    this.props.setFilter({ name: '', is_complete: 'both'})
  }

  handleSearch = name => this.props.setFilter({ name });

  handleFilter = filter => this.props.setFilter({ 'is_complete': filter });

  render() {
    const { todos, addTodo, match } = this.props;
    return (
      <div className={'todo-list-view'}>
        <Helmet title="Todo List" />
        <div className={'todo-list-view__header'}>
          Todos
        </div>
        <Paper style={CUSTOM_PAPER_STYLE}>
          <AddTodoModal listId={match.params.listId} onSubmit={addTodo} />
          <TodoFilter onChange={this.handleFilter} />
          <Search onChange={this.handleSearch} />
          <TodoList data={todos} />
        </Paper>
      </div>
    );
  }
}
