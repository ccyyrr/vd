import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { withRouter } from 'react-router-dom';

import { TodoListView } from './todoListView.component';
import { TodoActions } from '../../modules/todo/todo.redux.js';
import { selectFilteredTodos } from '../../modules/todo/todo.selectors.js';

const mapStateToProps = createStructuredSelector({
  todos: selectFilteredTodos,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  fetchTodos: TodoActions.fetchTodos,
  addTodo: TodoActions.addTodo,
  setFilter: TodoActions.setFilter,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TodoListView));