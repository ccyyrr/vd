import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { TodoItem } from './todoItem.component';
import { TodoActions } from '../../../modules/todo/todo.redux.js';

const mapStateToProps = createStructuredSelector({
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  removeTodo: TodoActions.removeTodo,
  updateTodo: TodoActions.updateTodo,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TodoItem);
