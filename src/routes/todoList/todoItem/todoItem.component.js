import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { ListItem } from 'material-ui/List';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Checkbox from 'material-ui/Checkbox';
import Done from 'material-ui/svg-icons/action/done';
import CropDin from 'material-ui/svg-icons/image/crop-din';
import DeleteForever from 'material-ui/svg-icons/action/delete';
import { red200 } from 'material-ui/styles/colors';

import{ EditModal } from '../../../components/editModal/editModal.component';
import{ ItemControlsWrapper } from '../../../components/itemControlsWrapper/itemControlsWrapper.component';

export class TodoItem extends PureComponent {
  static propTypes = {
    removeTodo: PropTypes.func.isRequired,
    updateTodo: PropTypes.func.isRequired,
    data: PropTypes.object,
  };

  onCheck = (e) => this.props.updateTodo({
    id: this.props.data.get('id'),
    is_complete: e.target.checked,
    name: this.props.data.get('name'),
    todo_list: this.props.data.get('todo_list'),
  });

  render() {
    const { data, removeTodo, updateTodo } = this.props;
    return (
      <ListItem
        leftCheckbox={
          <Checkbox
            checked={data.get('is_complete')}
            checkedIcon={<Done />}
            uncheckedIcon={<CropDin />}
            onCheck={this.onCheck}
          />
        }
        rightIconButton={
          <ItemControlsWrapper>
            <EditModal
              data={data}
              onSubmit={updateTodo}
            />
            <FloatingActionButton
              backgroundColor={red200}
              mini={true}
              onClick={() => removeTodo(data.get('id'))}
            >
              <DeleteForever />
            </FloatingActionButton>
          </ItemControlsWrapper>}
        primaryText={data.get('name')}
      />
    );
  }
}
