import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation';
import Paper from 'material-ui/Paper';
import Done from 'material-ui/svg-icons/action/done';
import List from 'material-ui/svg-icons/action/list';
import CropDin from 'material-ui/svg-icons/image/crop-din';

export const FILTER_VALUES = [ 'both', true, false ];

export class TodoFilter extends PureComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
  };

  state = {
    selectedIndex: 0,
    is_complete: false,
  };

  onSelect = index => {
    this.setState({ selectedIndex: index });
    this.props.onChange(FILTER_VALUES[index]);
  };

  render() {
    return (
      <Paper zDepth={1}>
        <BottomNavigation selectedIndex={this.state.selectedIndex}>
          <BottomNavigationItem
            label="All"
            icon={<List />}
            onClick={() => this.onSelect(0)}
          />
          <BottomNavigationItem
            label="Completed"
            icon={<Done />}
            onClick={() => this.onSelect(1)}
          />
          <BottomNavigationItem
            label="To do"
            icon={<CropDin />}
            onClick={() => this.onSelect(2)}
          />
        </BottomNavigation>
      </Paper>
    );
  }
}