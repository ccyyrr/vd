import React, { PureComponent } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';

import App from './app.container';
import Home from './home';
import TodoListView from './todoList';
import NotFound from './notFound';

export class RootContainer extends PureComponent {
  render() {
    return (
      <Switch>
        <Route exact path="/404" component={NotFound} />
        <Route path="/">
          <App>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/list/:listId" component={TodoListView} />
              <Route component={NotFound} />
            </Switch>
          </App>
        </Route>
      </Switch>
    );
  }
}

export default withRouter(RootContainer);
