import { combineReducers } from 'redux-immutable';

import { reducer as routerReducer } from './router/router.redux';
import { reducer as todoReducer } from './todo/todo.redux';

export default function createReducer() {
  return combineReducers({
    route: routerReducer,
    todo: todoReducer,
  });
}
