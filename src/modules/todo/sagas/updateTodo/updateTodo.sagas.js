import { all, call, put, takeLatest } from 'redux-saga/effects';

import { put as putApi } from '../../../api/api.sagas';
import { TodoTypes, TodoActions } from '../../todo.redux';

export function* updateTodo(action) {
  try {
    const data = yield call(putApi, `/todos/${action.data.id}/`, action.data);
    yield put(TodoActions.updateTodoSuccess(data));
  } catch (e) {
    yield put(TodoActions.updateTodoError(e));
  }}

export default function* updateTodoSagas() {
  yield all([
    takeLatest(TodoTypes.UPDATE_TODO, updateTodo),
  ]);
}
