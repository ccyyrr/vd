import { all, call, put, takeLatest } from 'redux-saga/effects';

import { post } from '../../../api/api.sagas';
import { TodoTypes, TodoActions } from '../../todo.redux';

export function* addTodo(action) {
  try {
    yield call(post, '/todos/', action.data);
    yield put(TodoActions.addTodoSuccess(action.data.todo_list));
  } catch (e) {
    yield put(TodoActions.addTodoError(e));
  }
}

export function* addTodoSuccess(action) {
  console.log(action)
  yield put(TodoActions.fetchTodos(action.data))
}

export default function* addTodoSagas() {
  yield all([
    takeLatest(TodoTypes.ADD_TODO_SUCCESS, addTodoSuccess),
    takeLatest(TodoTypes.ADD_TODO, addTodo),
  ]);
}
