import { all, call, put, takeLatest } from 'redux-saga/effects';

import { post } from '../../../api/api.sagas';
import { TodoTypes, TodoActions } from '../../todo.redux';

export function* addTodoList(action) {
  try {
    yield call(post, '/todolists/', action.data);
    yield put(TodoActions.addTodoListSuccess());
  } catch (e) {
    yield put(TodoActions.addTodoListError(e));
  }
}

export function* addTodoListSuccess() {
  yield put(TodoActions.fetchTodoLists())
}

export default function* addTodoListSagas() {
  yield all([
    takeLatest(TodoTypes.ADD_TODO_LIST_SUCCESS, addTodoListSuccess),
    takeLatest(TodoTypes.ADD_TODO_LIST, addTodoList),
  ]);
}
