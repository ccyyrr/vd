import { all, call, put, takeLatest } from 'redux-saga/effects';

import { del } from '../../../api/api.sagas';
import { TodoTypes, TodoActions } from '../../todo.redux';

export function* removeTodo(action) {
  try {
    yield call(del, `/todos/${action.id}/`);
    yield put(TodoActions.removeTodoSuccess(action.id));
  } catch (e) {
    yield put(TodoActions.removeTodoError(e));
  }
}

export default function* removeTodoSagas() {
  yield all([
    takeLatest(TodoTypes.REMOVE_TODO, removeTodo),
  ]);
}
