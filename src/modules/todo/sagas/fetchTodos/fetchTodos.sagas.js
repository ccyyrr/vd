import { all, call, put, takeLatest } from 'redux-saga/effects';

import { get } from '../../../api/api.sagas';
import { TodoTypes, TodoActions } from '../../todo.redux';

export function* fetchTodos(action) {
  try {
    const data = yield call(get, `/todolists/${action.id}/`);
    yield put(TodoActions.fetchTodosSuccess(data));
  } catch (e) {
    yield put(TodoActions.fetchTodosError(e));
  }
}

export default function* fetchTodosSagas() {
  yield all([
    takeLatest(TodoTypes.FETCH_TODOS, fetchTodos),
  ]);
}
