import { all, call, put, takeLatest } from 'redux-saga/effects';

import { get } from '../../../api/api.sagas';
import { TodoTypes, TodoActions } from '../../todo.redux';

export function* fetchTodoLists() {
  try {
    const data = yield call(get, '/todolists/');
    yield put(TodoActions.fetchTodoListsSuccess(data));
  } catch (e) {
    yield put(TodoActions.fetchTodoListsError(e));
  }
}

export default function* fetchTodoListsSagas() {
  yield all([
    takeLatest(TodoTypes.FETCH_TODO_LISTS, fetchTodoLists),
  ]);
}
