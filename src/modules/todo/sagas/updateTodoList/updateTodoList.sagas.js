import { all, call, put, takeLatest } from 'redux-saga/effects';

import { put as putApi } from '../../../api/api.sagas';
import { TodoTypes, TodoActions } from '../../todo.redux';

export function* updateTodoList(action) {
  try {
    const data = yield call(putApi, `/todolists/${action.data.id}/`, action.data);
    yield put(TodoActions.updateTodoListSuccess(data));
  } catch (e) {
    yield put(TodoActions.updateTodoListError(e));
  }
}


export default function* updateTodoListSagas() {
  yield all([
    takeLatest(TodoTypes.UPDATE_TODO_LIST, updateTodoList),
  ]);
}
