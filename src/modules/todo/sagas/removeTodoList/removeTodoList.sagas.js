import { all, call, put, takeLatest } from 'redux-saga/effects';

import { del } from '../../../api/api.sagas';
import { TodoTypes, TodoActions } from '../../todo.redux';

export function* removeTodoList(action) {
  try {
    yield call(del, `/todolists/${action.id}/`);
    yield put(TodoActions.removeTodoListSuccess(action.id));
  } catch (e) {
    yield put(TodoActions.removeTodoListError(e));
  }
}

export default function* removeTodoListSagas() {
  yield all([
    takeLatest(TodoTypes.REMOVE_TODO_LIST, removeTodoList),
  ]);
}
