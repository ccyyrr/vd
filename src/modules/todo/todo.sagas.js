import { all } from 'redux-saga/effects';

import fetchTodoListsSagas from './sagas/fetchTodoLists/fetchTodoLists.sagas';
import removeTodoListSagas from './sagas/removeTodoList/removeTodoList.sagas';
import removeTodoSagas from './sagas/removeTodo/removeTodo.sagas';
import addTodoListSagas from './sagas/addTodoList/addTodoList.sagas';
import addTodoSagas from './sagas/addTodo/addTodo.sagas';
import fetchTodosSagas from './sagas/fetchTodos/fetchTodos.sagas';
import updateTodoSagas from './sagas/updateTodo/updateTodo.sagas';
import updateTodoListSagas from './sagas/updateTodoList/updateTodoList.sagas';

export default function* todoSagas() {
  yield all ([
    addTodoListSagas(),
    addTodoSagas(),
    removeTodoListSagas(),
    removeTodoSagas(),
    fetchTodoListsSagas(),
    fetchTodosSagas(),
    updateTodoSagas(),
    updateTodoListSagas(),
  ]);
}
