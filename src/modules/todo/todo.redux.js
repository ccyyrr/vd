import { createActions, createReducer } from 'reduxsauce';
import { Record, List, fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';

export const { Types: TodoTypes, Creators: TodoActions } = createActions({
  fetchTodoLists: [],
  fetchTodoListsSuccess: ['data'],
  fetchTodoListsError: ['payload'],
  removeTodoList: ['id'],
  removeTodoListSuccess: ['id'],
  removeTodoListError: ['payload'],
  addTodoList: ['data'],
  addTodoListSuccess: ['data'],
  addTodoListError: ['payload'],
  updateTodoList: ['data'],
  updateTodoListSuccess: ['data'],
  updateTodoListError: ['payload'],
  fetchTodos: ['id'],
  fetchTodosSuccess: ['data'],
  fetchTodosError: ['payload'],
  removeTodo: ['id'],
  removeTodoSuccess: ['id'],
  removeTodoError: ['payload'],
  addTodo: ['data'],
  addTodoSuccess: ['data'],
  addTodoError: ['payload'],
  updateTodo: ['data'],
  updateTodoSuccess: ['data'],
  updateTodoError: ['payload'],
  setFilter: ['filter'],
}, { prefix: 'TODO_' });

const TodoRecord = new Record({
  lists: List(),
  todos: List(),
  filter: fromJS({ name: '' }),
});

export const INITIAL_STATE = new TodoRecord({});

const getTodoListsSuccessHandler = (state = INITIAL_STATE, action) => state.set('lists', fromJS(action.data));

const getTodosSuccessHandler = (state = INITIAL_STATE, action) => state.set('todos', fromJS(action.data));

const setFilterHandler = (state = INITIAL_STATE, action) => state.mergeDeep({ filter: fromJS(action.filter) });

const updateTodoHandler = (state = INITIAL_STATE, action) => {
  const updatedObject = fromJS(action.data);
  return state.set('todos', state.get('todos').map(item => {
    if(item.get('id') === action.data.id) {
      return updatedObject;
    }
    return item;
  }));
}

const removeTodoHandler = (state = INITIAL_STATE, action) => {
  return state.set('todos', state.get('todos').filter(item => 
    (item.get('id') !== action.id)));
}

const updateTodoListHandler = (state = INITIAL_STATE, action) => {
  const updatedObject = fromJS(action.data);
  return state.set('lists', state.get('lists').map(item => {
    if(item.get('id') === action.data.id) {
      return updatedObject;
    }
    return item;
  }));
}

const removeTodoListHandler = (state = INITIAL_STATE, action) => {
  return state.set('lists', state.get('lists').filter(item => 
    (item.get('id') !== action.id)));
}

const handleClear = (state = INITIAL_STATE, action) => state.set('todos', List());

export const reducer = createReducer(INITIAL_STATE, {
  [TodoTypes.FETCH_TODO_LISTS_SUCCESS]: getTodoListsSuccessHandler,
  [TodoTypes.FETCH_TODOS_SUCCESS]: getTodosSuccessHandler,
  [TodoTypes.UPDATE_TODO_SUCCESS]: updateTodoHandler,
  [TodoTypes.REMOVE_TODO_SUCCESS]: removeTodoHandler,
  [TodoTypes.UPDATE_TODO_LIST_SUCCESS]: updateTodoListHandler,
  [TodoTypes.REMOVE_TODO_LIST_SUCCESS]: removeTodoListHandler,
  [TodoTypes.SET_FILTER]: setFilterHandler,
  [LOCATION_CHANGE]: handleClear,
})