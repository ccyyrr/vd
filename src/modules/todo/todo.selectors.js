import { createSelector } from 'reselect';

const selectTodoDomain = state => state.get('todo');

export const selectTodoLists = createSelector(
  selectTodoDomain, state => state.get('lists')
);

export const selectCurrentTodoList = createSelector(
  selectTodoDomain, state => state.get('todos')
);

export const selectFilteredLists = createSelector(
  selectTodoDomain,
  state => state.get('lists').filter(e => (e.get('name')
      .toLowerCase().indexOf(state.getIn(['filter', 'name']).toLowerCase()) > -1))
);

export const selectFilteredTodos = createSelector(
  selectTodoDomain,
  state => state.get('todos')
    .filter(e => e
      .get('name')
      .toLowerCase()
      .indexOf(state.getIn(['filter', 'name']).toLowerCase()) > -1)
    .filter(e => {
      if(state.getIn(['filter', 'is_complete']) === 'both') {
        return true;
      }
      return e.get('is_complete') === state.getIn(['filter', 'is_complete']);
    })
);
