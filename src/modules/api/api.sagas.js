import { call } from 'redux-saga/effects';
import { assign } from 'lodash';
import { stringify } from 'qs';

export const API_ROOT = 'https://todos.venturedevs.net/api';

export function parseJSON(response) {
  if(!response.body) {
    return true;
  }
  return response.json();
}

export function* requestSaga(url, options) {
  const headers = {
    'Content-Type': 'application/json',
  };
  const fetchUrl = `${API_ROOT}${url}`;
  try {
    const response = yield call(fetch, fetchUrl, assign({ headers }, options));
    return yield call(parseJSON, response);
  } catch (e) {
    return yield call(parseJSON, e);
  }
}

export function* get(url, body = {}, options = {}) {
  return yield call(requestSaga, `${url}?${stringify(body)}`, assign({ method: 'GET' }, options));
}

export function* del(url, body = {}, options = {}) {
  return yield call(requestSaga, `${url}?${stringify(body)}`, assign({ method: 'DELETE' }, options));
}

export function* put(url, body = {}, options = {}) {
  return yield call(requestSaga, url, assign({ method: 'PUT', body: JSON.stringify(body) }, options));
}

export function* patch(url, body = {}, options = {}) {
  return yield call(requestSaga, url, assign({ method: 'PATCH', body: JSON.stringify(body) }, options));
}

export function* post(url, body = {}, options = {}) {
  return yield call(requestSaga, url, assign({ method: 'POST', body: JSON.stringify(body) }, options));
}
