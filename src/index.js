import React from 'react';
import ReactDOM from 'react-dom';
// Needed for redux-saga es6 generator support
import 'babel-polyfill';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Provider } from 'react-redux';
import createHistory from 'history/createBrowserHistory';
import { ConnectedRouter } from 'react-router-redux';
import 'normalize.css/normalize.css';
import './index.css';
import configureStore from './modules/store';

const initialState = {};
const browserHistory = createHistory();
const store = configureStore(initialState, browserHistory);

const render = () => {
  const NextApp = require('./routes').default;

  ReactDOM.render(
    <Provider store={store}>
      <ConnectedRouter history={browserHistory}>
        <MuiThemeProvider>
          <NextApp />
        </MuiThemeProvider>
      </ConnectedRouter>
    </Provider>,
    document.getElementById('root')
  );
};

render();