import React from 'react';
import FlatButton from 'material-ui/FlatButton';

export const getActions = (handleCancel, handleSubmit, isDisabled) => {
    return ([
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={handleCancel}
      />,
      <FlatButton
        disabled={isDisabled}
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onClick={handleSubmit}
      />,
    ]);
  };