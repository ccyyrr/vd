export const ERROR_TEXT = 'This field is required';

export const validateText = ({target: { value }}, setState) => {
  setState({ value, error: '', isDisabled: false });
  if (value.trim().length <= 0) {
    setState({ error: ERROR_TEXT, isDisabled: true })
  }
};